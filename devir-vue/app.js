let url = "http://localhost:3006";

var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue Again!',
        workers:[],
        info:"",
        worker:{
        	name:"",
        	surname:"",
        	position:"",
        	experience:0,
        }
    },
    methods:{
    	insertUser:function(){
    		console.log(this.worker)

    		axios.post(url+"/worker/insert",{
    			name:this.worker.name,
    			surname:this.worker.surname,
    			position:this.worker.position,
    			experience:this.worker.experience,
    		},(res)=>{
    			console.log(res)
    		}).catch(function (error) {
		    console.log(error);
		})

    	}
    },
    mounted:function(){
		axios.get(url+"/worker").then((res)=>{
			console.log(res.data.workers);
			this.workers = res.data.workers;
		}).catch(function (error) {
		    console.log(error);
		})

		axios.get(url+'/info').then((res)=>{
			console.log(res.data.infoList);
			this.info = res.data.infoList[0];
		}).catch(function (error) {
		    console.log(error);
		})		
    }
})
