var express = require('express');
var router = express.Router();

let Worker = require("../models/Worker.js")
let db = require("../db.js");


router.get('/', function(req, res, next) {
	Worker.find({},function(err,workerList){
		if(err){
			console.log(err)
			res.json({error:err});
		}
		console.log(workerList)
		res.json({workers:workerList});
	})
});

router.post('/insert',(req,res,next)=>{
	let {name,surname,position,image,experience} = req.body;

	Worker.create({name,surname,position,image,experience},(err,newWorker)=>{
		if(err){
			console.log(err)
		}
		res.json({newWorker});
	})
})

module.exports = router;
