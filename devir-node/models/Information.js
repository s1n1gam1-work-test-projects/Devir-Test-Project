var mongoose = require('mongoose');
let db = require("../db.js")

//Define a schema
var Schema = mongoose.Schema;

var InformationSchema = new Schema({
    info_title: String,
    info_main: String
});

module.exports = db.model("infos",InformationSchema)
