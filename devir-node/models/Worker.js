var mongoose = require('mongoose');
let db = require("../db.js")

//Define a schema
var Schema = mongoose.Schema;

var WorkerSchema = new Schema({
    name: {
    	type:String,
    	required:true,
    },
    surname:{
    	type:String,
    	required:false,
    	default:"No surname"
    },
    position:{
    	type:String,
    	required:false,
    	default:"Неизвестно"
    },
    image:{
    	type:String,
    	required:false,
    	default:"noimage.png"
    },
    experience:{
    	type:Number,
    	required:false,
    	default:0
    }

});

module.exports = db.model("workers",WorkerSchema)